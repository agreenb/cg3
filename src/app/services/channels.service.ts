import {Injectable, OnInit} from '@angular/core';
import {Channel} from '../models/channel';
import {Observable, of, pipe} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {filter} from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  private channelsUrl = 'api/channels';


  getChannels(): Observable<Channel[]> {
    return this.http.get<Channel[]>(this.channelsUrl);
  }



  constructor(private http: HttpClient) { }

}
