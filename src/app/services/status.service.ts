import { Injectable } from '@angular/core';
import {Plan} from '../models/plan';
import {Category} from '../models/category';
import {Channel} from '../models/channel';
import {Genre} from '../models/genre';

export interface Status {
  plan?: Plan;
  categories?: Category[];
  selectedCategory?: Category;
  selectedGenre?: Genre;
  genres?: Genre[];
  channels?: Channel[];
}
// export interface CurrentSelection {
//   plan: string;
//   category: string;
//   genre: string;
// }

@Injectable({
  providedIn: 'root'
})
export class StatusService {
  currentSelection: Status;


  selectCategory(category: Category){
    // this.currentSelection.category = category;
  }

  addGenres(genre: Genre){
    // this.currentSelection.genres.push(genre);
  }

  selectGenre(genre: Genre){
    // this.currentSelection.selectedGenre = genre;
  }

  addChannels(channel: Channel){
    // this.currentSelection.channels.push(channel);
  }



}
