import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Plan} from '../models/plan';

@Injectable({
  providedIn: 'root'
})
export class PlansService {

  private plansUrl = 'api/plans';

  getPlans(): Observable<Plan[]> {
    return this.http.get<Plan[]>(this.plansUrl);
  }


  constructor(public http: HttpClient) { }
}
