import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Genre} from '../models/genre';
import {filter, flatMap, map} from 'rxjs/operators';
import {StatusService} from './status.service';
import {Category} from '../models/category';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class GenresService {

  private genresUrl = 'api/genres';

  constructor(private http: HttpClient, public statusService: StatusService) { }

  getGenres(category: string): Observable<Genre[]> {
    this.statusService.currentSelection.genres
    return this.http.get<Genre[]>(this.genresUrl)
      .pipe(
        map(genres => genres.filter(genre => genre.category === category));
      );
  }



}
