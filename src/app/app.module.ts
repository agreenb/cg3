import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GenresComponent } from './components/genres/genres.component';
import { ChannelsComponent } from './components/channels/channels.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { PlansComponent } from './components/plans/plans.component';
import { AppMaterialModule } from './modules/app-material.module';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import { DatabaseService} from './services/database.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    GenresComponent,
    ChannelsComponent,
    CategoriesComponent,
    PlansComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      DatabaseService, { dataEncapsulation: false }
    )

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  private static AppRoutingModule: any;
}
