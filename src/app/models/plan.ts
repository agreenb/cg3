export class Plan {
  name: string;
  key: string;
  price: number;
  subscription: string;
}
