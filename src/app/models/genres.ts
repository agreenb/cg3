import {Genre} from './genre';

export const GENRES: Genre[] = [
  {name: 'All Music', key: 'All Music', category: 'music'},
  {name: 'Rock', key: 'Rock', category: 'music'},
  {name: 'Pop', key: 'Pop', category: 'music'},
  {name: 'Country', key: 'Country', category: 'music'},
  {name: 'Hip-Hop/R&B', key: 'RandB_Hip-Hop', category: 'music'},
  {name: 'Christian', key: 'Christian', category: 'music'},
  {name: 'Jazz/Standards', key: 'Jazz/Standards', category: 'music'},
  {name: 'Classical', key: 'Classical', category: 'music'},
  {name: 'Dance/Electronic', key: 'Dance/Electronic', category: 'music'},
  {name: 'Latino', key: 'Latino', category: 'music'},
  {name: 'Holiday', key: 'Holiday', category: 'music'},
  {name: 'Canadian', key: 'Canadian', category: 'music'},

  {name: 'Sports', key: 'Sports', category: 'sports'},
  {name: 'NFL Play-by-Play', key: 'NFL Play-by-Play', category: 'sports'},
  {name: 'MLB Play-by-Play', key: 'MLB Play-by-Play', category: 'sports'},
  {name: 'College Play-by-Play', key: 'College Play-by-Play', category: 'sports'},
  {name: 'NBA Play-by-Play', key: 'NBA Play-by-Play', category: 'sports'},
  {name: 'NHL Play-by-Play', key: 'NHL Play-by-Play', category: 'sports'},
  {name: 'Sports Play-by-Play', key: 'Sports Play-by-Play', category: 'sports'},

  {name: 'All Talk', key: 'talk', category: 'talk'},
  {name: 'Entertainment', key: 'Entertainment', category: 'talk'},
  {name: 'Comedy', key: 'Comedy', category: 'talk'},
  {name: 'Religion', key: 'Religion', category: 'talk'},
  {name: 'Kids', key: 'Kids', category: 'talk'},
  {name: 'More', key: 'More', category: 'talk'},

  {name: 'All News', key: 'all_news', category: 'news'},
  {name: 'News/Public Radio', key: 'news_public_radio', category: 'news'},
  {name: 'Politics/Issues', key: 'Politics/Issues', category: 'news'},
  {name: 'Traffic/Weather', key: 'Traffic/Weather', category: 'news'},

  {name: 'Howard Stern', key: 'howard', category: 'howard'},

  {name: 'All', key: 'all', category: 'all'},
]
