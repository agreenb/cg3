import {Plan} from './plan';

export const PLANS: Plan[] = [
  {
    name: 'SiriusXM Select',
    key: 'SiriusXMSelect',
    price: 15.99,
    subscription: 'https://care.siriusxm.com/login_view.action?pkg=SE',
  },
  {
    name: 'SiriusXM Mostly Music',
    key: 'SiriusXMMostlyMusic',
    price: 10.99,
    subscription: 'https://care.siriusxm.com/login_view.action?pkg=MM',
  },
  {
    name: 'SiriusXM All Access Package',
    key: 'siriusxmallaccess',
    price: 19.99,
    subscription: 'https://care.siriusxm.com/login_view.action?pkg=AA',
  }
];
