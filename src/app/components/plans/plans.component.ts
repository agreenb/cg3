import { Component, OnInit } from '@angular/core';
import {PlansService} from '../../services/plans.service';
import {Plan} from '../../models/plan';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {

  title = 'Channel Guide'
  selectedPlan: Plan;
  plans: Plan[];

  getPlans(): void{
    this.plansService.getPlans()
      .subscribe(plans => this.plans = plans);
    if (!this.selectedPlan && this.plans) {
      this.selectedPlan = this.plans[0];
    }
  }

  constructor(public plansService: PlansService) { }

  ngOnInit() {
    this.getPlans();
  }

}
