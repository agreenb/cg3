import { Component, OnInit } from '@angular/core';
import {GenresService} from '../../services/genres.service';
import {Genre} from '../../models/genre';
import {Category} from '../../models/category';
import {StatusService} from '../../services/status.service';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss']
})
export class GenresComponent implements OnInit {

  selectedGenre: Genre;
  genres: Genre[];

  getGenres(category: string): void {
    this.genresService.getGenres(category)
      .subscribe(genres => this.statusService.currentSelection.genres = genres);
    this.statusService.currentSelection.selectedGenre = this.statusService.currentSelection.genres[0];


    // if (!this.selectedGenre && this.genres) {
    //   this.selectedGenre = this.genres[0];
    // }
  }


  constructor(private genresService: GenresService, public statusService: StatusService) { }


  ngOnInit() {
    this.getGenres('sports');

  }

}
