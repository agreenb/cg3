import { Component, OnInit } from '@angular/core';
import {CategoriesService} from '../../services/categories.service';
import {Category} from '../../models/category';
import {MatTabChangeEvent} from '@angular/material';
import {StatusService} from '../../services/status.service';
import {filter} from 'rxjs/operators';
import {GenresService} from '../../services/genres.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  selectedCategoryIndex: number;
  selectedCategory: Category;
  categories: Category[];

  updateCategoryIndex(event: MatTabChangeEvent): void{
    console.log(event);
  }

  getCategories(): void {
    this.categoriesService.getCategories()
      .subscribe(categories => this.categories = categories);

    if (!this.selectedCategory && this.categories){
      this.selectedCategory = this.categories[0];
    }
    if (this.categories){
      this.selectedCategory = this.categories[this.selectedCategoryIndex];
      this.genreService.getGenres(this.selectedCategory.key);
    }


  }

  constructor(public categoriesService: CategoriesService, public statusService: StatusService, public genreService: GenresService){}

  ngOnInit() {
    this.getCategories();
    // this.selectedCategoryIndex = 2;


  }
}
