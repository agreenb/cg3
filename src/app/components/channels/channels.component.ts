import { Component, OnInit } from '@angular/core';
import {ChannelService} from '../../services/channels.service';
import {Channel} from '../../models/channel';
import {filter} from 'rxjs/operators';
import {GenresService} from '../../services/genres.service';
import {StatusService} from '../../services/status.service';

@Component({
  selector: 'app-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.scss']
})
export class ChannelsComponent implements OnInit {

  channels: Channel[];
  constructor(public channelService: ChannelService) { }

  getChannels(): void {
    this.channelService.getChannels()
      .subscribe(channels => this.channels = channels.slice(0, 20));
  }


  // this.channels = channels.filter(channel => channel.progtypetitle === this.genres[this.selectedGenre].name));

  ngOnInit() {
    this.getChannels();
  }

}
